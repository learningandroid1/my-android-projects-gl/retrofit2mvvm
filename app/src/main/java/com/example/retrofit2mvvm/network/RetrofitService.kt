package com.example.retrofit2mvvm.network

import com.example.retrofit2mvvm.model.response.GithubResponse
import retrofit2.Call
import retrofit2.http.GET

interface RetrofitService {
    @GET("/repos/octocat/hello-world/issues")
    fun getData(): Call<List<GithubResponse>>
}