package com.example.retrofit2mvvm.model.response

data class GithubResponse (
    val title: String,
    val created_at: String
)