package com.example.retrofit2mvvm.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.retrofit2mvvm.model.response.GithubResponse
import com.example.retrofit2mvvm.network.RetrofitInstance
import com.example.retrofit2mvvm.network.RetrofitService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.create

class GithubViewModel: ViewModel() {

    var githubDataList = MutableLiveData<List<GithubResponse>>()

    fun getApiData(){
        val retrofitService = RetrofitInstance.getRetrofitInstance().create(RetrofitService::class.java)
        retrofitService.getData().enqueue(object: Callback<List<GithubResponse>>{
            override fun onResponse(
                call: Call<List<GithubResponse>>,
                response: Response<List<GithubResponse>>,
            ) {
                githubDataList.value = response.body()
            }

            override fun onFailure(call: Call<List<GithubResponse>>, t: Throwable) {
            }
        })
    }
}