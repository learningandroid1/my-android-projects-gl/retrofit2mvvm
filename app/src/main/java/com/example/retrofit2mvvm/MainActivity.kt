package com.example.retrofit2mvvm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.retrofit2mvvm.adapter.GitHubAdapter
import com.example.retrofit2mvvm.model.response.GithubResponse
import com.example.retrofit2mvvm.viewmodel.GithubViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var githubViewModel: GithubViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initGithubApi()
    }

    private fun initAdapter(data: List<GithubResponse>){
        rvGithubList.layoutManager = LinearLayoutManager(this)
        val adapter = GitHubAdapter(data)
        rvGithubList.adapter = adapter
    }

    fun initGithubApi(){
        githubViewModel = ViewModelProvider(this).get(GithubViewModel::class.java)
        // call github api from viewmodel
        githubViewModel.getApiData()
        // Livedata observer
        githubViewModel.githubDataList.observe(this, Observer {
            initAdapter(it)
        })
    }
}